﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        // open create user form
        frmCreateUser createUserForm = new frmCreateUser();

        private void btnCreateUser_Click(object sender, EventArgs e)
        {
            createUserForm.Show();
            Hide();

            //logging
            core.Logger.Instance.Log("frmMain: btnCreateUser_Click");
        }

        // open patient form
        frmPatientFind patientFormFind = new frmPatientFind();

        private void btnPatient_Click(object sender, EventArgs e)
        {
            patientFormFind.Show();
            Hide();

            //logging
            core.Logger.Instance.Log("frmMain: btnPatient_Click");
        }

        // open daily appointment form
        frmStaffSchedule staffScheduleForm = new frmStaffSchedule();

        private void btnStaffSch_Click(object sender, EventArgs e)
        {
            staffScheduleForm.Show();
            Hide();

            //logging
            core.Logger.Instance.Log("frmMain: btnStaffSch_Click");
        }

        // open find appointments form

        frmAppointments appointmentsFindForm = new frmAppointments();

        private void btnFindApp_Click(object sender, EventArgs e)
        {
            appointmentsFindForm.Show();
            Hide();

            //logging
            core.Logger.Instance.Log("frmMain: btnFindApp_Click");
        }

        // exit menu and close the application, but could change to return to the login menu

        

        private void btnExit_Click(object sender, EventArgs e)
        {
            //loginForm.Show();

            // close the application options
            Application.Exit();
        }


        frmDailyAppointments dailyAppointmentsForm = new frmDailyAppointments();
        private void btnDailyApp_Click(object sender, EventArgs e)
        {
            dailyAppointmentsForm.Show();
            Hide();

            //logging
            core.Logger.Instance.Log("frmMain: btnDailyApp_Click");
        }
    }
}

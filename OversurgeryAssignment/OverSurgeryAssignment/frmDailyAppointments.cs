﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmDailyAppointments : Form
    {
        public frmDailyAppointments()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        // Update appointments when date has changed
        private void date_changed(object sender, EventArgs e)
        {
            txtDocNurse.Text = String.Empty;

            //To enable date change from SQL as was in the wrong format
            String dateString = "'" + dtpDailyApp.Value.ToString("yyyy-MM-dd") + "'";

            String selectString = Constants.getDailyAppointments.Replace("@Date", dateString);

            //logging
            core.Logger.Instance.Log("frmDailyAppointments: dtpDailyApp - Date Changed");

            DataSet dsAppointments = DBConnection.getDBConnectionInstance().getDataSet(selectString);

            DataTable dtAppointments = dsAppointments.Tables[0];

            dgdDailyAppView.DataSource = dtAppointments;

            dgdDailyAppView.Update();

            dgdDailyAppView.Refresh();

        }
        // take back to the Main Menu
        private void btnHome_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMain mainForm = new frmMain();
            mainForm.Show();

            //logging
            core.Logger.Instance.Log("frmDailyAppointments: btnHome_Click");


        }


        private void btnDocNurseSearch_Click(object sender, EventArgs e)
        {
            String docNurse = txtDocNurse.Text;

            if (txtDocNurse.Text == "")
            {
                this.date_changed(sender, e);
            }

            else
            {
                String selectString = Constants.getDocAppointments;
                selectString = selectString.Replace("@DocNurse", docNurse);
                String dateString = "'" + dtpDailyApp.Value.ToString("yyyy-MM-dd") + "'";

                selectString = selectString.Replace("@Date", dateString);

                //logging
                core.Logger.Instance.Log("frmDailyAppointments: btnDocNurseSearch_Click - search ");

                DataSet dsDocAppointments = DBConnection.getDBConnectionInstance().getDataSet(selectString);

                DataTable dtDocAppointments = dsDocAppointments.Tables[0];

                dgdDailyAppView.DataSource = dtDocAppointments;

                dgdDailyAppView.Update();

                dgdDailyAppView.Refresh();
            }

        }

        /*private void txtDocNurse_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                btnDocNurseSearch.PerformClick();
            }
        }*/
                
    }
}

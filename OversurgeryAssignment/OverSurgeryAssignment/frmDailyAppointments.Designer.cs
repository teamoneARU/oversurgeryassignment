﻿namespace OverSurgeryAssignment
{
    partial class frmDailyAppointments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dtpDailyApp = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dgdDailyAppView = new System.Windows.Forms.DataGridView();
            this.btnHome = new System.Windows.Forms.Button();
            this.txtDocNurse = new System.Windows.Forms.TextBox();
            this.btnDocNurseSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgdDailyAppView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(30, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Appointments";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // dtpDailyApp
            // 
            this.dtpDailyApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDailyApp.Location = new System.Drawing.Point(336, 69);
            this.dtpDailyApp.Name = "dtpDailyApp";
            this.dtpDailyApp.Size = new System.Drawing.Size(200, 24);
            this.dtpDailyApp.TabIndex = 1;
            this.dtpDailyApp.ValueChanged += new System.EventHandler(this.date_changed);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(84, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(308, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Choose Doctor or Nurse or leave blank for all";
            // 
            // dgdDailyAppView
            // 
            this.dgdDailyAppView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgdDailyAppView.Location = new System.Drawing.Point(37, 196);
            this.dgdDailyAppView.Name = "dgdDailyAppView";
            this.dgdDailyAppView.Size = new System.Drawing.Size(762, 229);
            this.dgdDailyAppView.TabIndex = 4;
            // 
            // btnHome
            // 
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.Location = new System.Drawing.Point(724, 21);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(83, 33);
            this.btnHome.TabIndex = 5;
            this.btnHome.Text = "Home";
            this.btnHome.UseVisualStyleBackColor = true;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // txtDocNurse
            // 
            this.txtDocNurse.Location = new System.Drawing.Point(424, 131);
            this.txtDocNurse.Name = "txtDocNurse";
            this.txtDocNurse.Size = new System.Drawing.Size(100, 20);
            this.txtDocNurse.TabIndex = 2;
            // 
            // btnDocNurseSearch
            // 
            this.btnDocNurseSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDocNurseSearch.Location = new System.Drawing.Point(576, 124);
            this.btnDocNurseSearch.Name = "btnDocNurseSearch";
            this.btnDocNurseSearch.Size = new System.Drawing.Size(85, 28);
            this.btnDocNurseSearch.TabIndex = 3;
            this.btnDocNurseSearch.Text = "Search";
            this.btnDocNurseSearch.UseVisualStyleBackColor = true;
            this.btnDocNurseSearch.Click += new System.EventHandler(this.btnDocNurseSearch_Click);
            // 
            // frmDailyAppointments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 461);
            this.Controls.Add(this.btnDocNurseSearch);
            this.Controls.Add(this.txtDocNurse);
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.dgdDailyAppView);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtpDailyApp);
            this.Controls.Add(this.label1);
            this.Name = "frmDailyAppointments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Over Surgery Daily Appointments";
            ((System.ComponentModel.ISupportInitialize)(this.dgdDailyAppView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpDailyApp;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgdDailyAppView;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.TextBox txtDocNurse;
        private System.Windows.Forms.Button btnDocNurseSearch;
    }
}
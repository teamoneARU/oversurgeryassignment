﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class button : UserControl
    {
        Form openForm;
        Form closeForm;

        [Browsable(false)]
        [Description("Specify the form that you want to open"), Category("Data")]
        public Form OpenForm { get => openForm; set => openForm = value; }

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Bindable(false)]
        public override string Text {
            get => button1.Text;
            set {
                button1.Text = value;

                if (DesignMode)
                    button1.Invalidate();
            }
        }

        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Bindable(false)]
        public FlatStyle flatStyle {
            get {
                return button1.FlatStyle;
            }
            set {
                button1.FlatStyle = value;

                if (DesignMode)
                    button1.Invalidate();
            }
        }

        public button()
        {
            InitializeComponent();
        }

        public void btnClick(object sender, EventArgs e) {
            closeForm = (Form)this.Parent;

            closeForm.Hide();
            OpenForm.Closed += (s, args) => closeForm.Close();
            OpenForm.Show();
        }
    }
}

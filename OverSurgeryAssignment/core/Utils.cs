﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace core
{
    public class Utils
    {
        public static int countRows(SqlDataReader reader)
        {
            int count = 0;

            while (reader.Read()) {
                count++;
            }

            return count;
        }

        public static void openWindow(Form current, Form next)
        {
            current.Hide();
            next.Closed += (s, args) => current.Close();
            next.Show();
        }

        public static DateTime getWeekBeginningDate(DateTime date_before) {
            DateTime date_after = new DateTime();
            DayOfWeek dayOfWeek = date_before.DayOfWeek;

            StringBuilder sb = new StringBuilder();
            sb.Append("Before: " + date_before.ToString("dd/MM/yyyy") + "\n");

            switch (dayOfWeek) {
                case DayOfWeek.Monday:
                    break;
                case DayOfWeek.Tuesday:
                    date_after = date_before.AddDays(-1);
                    break;
                case DayOfWeek.Wednesday:
                    date_after = date_before.AddDays(-2);
                    break;
                case DayOfWeek.Thursday:
                    date_after = date_before.AddDays(-3);
                    break;
                case DayOfWeek.Friday:
                    date_after = date_before.AddDays(-4);
                    break;
                case DayOfWeek.Saturday:
                    date_after = date_before.AddDays(-5);
                    break;
                case DayOfWeek.Sunday:
                    date_after = date_before.AddDays(-6);
                    break;
            }

            return date_after;
        }
    }
}

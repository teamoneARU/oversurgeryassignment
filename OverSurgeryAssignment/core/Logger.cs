﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace core
{
    public class Logger
    {
        private static Logger _instance;
        private string filePath;

        private Logger() {
            filePath = Directory.GetCurrentDirectory() + "\\log";
        }

        public static Logger Instance {
            get {
                if (_instance == null)
                    _instance = new Logger();

                return _instance;
            }
        }

        public void Log(string m) {
            string message = DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " " + m;

            using (StreamWriter writer = new StreamWriter(filePath, true)) {
                writer.WriteLine(message);
                writer.Close();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace core
{
    public class Validation
    {
        public static Boolean validate(string input)
        {
            string pattern = "^[a-zA-Z]+[\\d]+";

            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(input);

            if (matches.Count > 0) {
                return true;
            }

            return false;
        }

    }

}
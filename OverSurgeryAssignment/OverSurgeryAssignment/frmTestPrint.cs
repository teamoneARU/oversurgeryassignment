﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmTestPrint : Form
    {
        int? ID;
        /*
         //opening the form with the test details pre filled
         
        private int? patientID;
        DataTable dtPrescriptions;


        public frmPrescription(int? patientID)
        {
            InitializeComponent();
            this.patientID = patientID;
        }
        */

        public frmTestPrint(int? ID)
        {
            InitializeComponent();
            this.ID = ID;
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            String testNo = txtTestID.Text;

            String selectString = Constants.getTestPrint;
            selectString = selectString.Replace("@TestNo", testNo);

            //logging
            core.Logger.Instance.Log("frmTestPrint: btnView_Click - Search Test" + testNo );

            DataSet dsTestPrint = DBConnection.getDBConnectionInstance().getDataSet(selectString);
            DataTable dtTestPrint = dsTestPrint.Tables[0];

            this.txtResult.AutoSize = true;

            txtTestDescription.Text = dsTestPrint.Tables[0].Rows[0]["testDescription"].ToString();
            txtResult.Text = dsTestPrint.Tables[0].Rows[0]["testResult"].ToString();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmTestResults testResultForm = new frmTestResults(ID);
            testResultForm.Show();

            //logging
            core.Logger.Instance.Log("frmTestPrint: btnHome_Click");
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMain mainForm = new frmMain();
            mainForm.Show();

            //logging
            core.Logger.Instance.Log("frmTestPrint: btnHome_Click");
        }

        private void TestName_Click(object sender, EventArgs e)
        {

        }

        private void frmTestPrint_Load(object sender, EventArgs e)
        {
            TestResult t = new TestResult();

            String testID = Constants.getTestID;
            testID = testID.Replace("@id", ID.ToString());
            String testDesc = Constants.getTestDescription;
            testDesc = testDesc.Replace("@id", ID.ToString());
            String testResult = Constants.getTestResult;
            testResult = testResult.Replace("@id", ID.ToString());

            //Update record in database
            DBConnection connection = DBConnection.getDBConnectionInstance();

            // Open the connection
            connection.openConnection();

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("@id", ID.ToString());

            SqlDataReader reader = connection.getPatientTests(Constants.getPatientTests, parameters);
            DataTable tests = new DataTable();
            tests.Load(reader);

            t.testID = (int?)tests.Rows[0]["testID"];
            t.description = (string)tests.Rows[0]["testDescription"];
            t.result = (string)tests.Rows[0]["testResult"];

            //DataTable dt1 = new DataTable();
            //dt1.Load(connection.returnID(testID));
            //t.testID = (int?)dt1.Rows[0]["testID"];

            //DataTable dt2 = new DataTable();
            //dt2.Load(connection.returnDesc(testID));
            //t.description = (string)dt2.Rows[0]["testDescription"];

            //DataTable dt3 = new DataTable();
            //dt3.Load(connection.returnID(testID));
            //t.result = (string)dt3.Rows[0]["testResult"];

            connection.closeConnection();

            txtTestID.Text = t.testID.ToString();
            txtTestDescription.Text = t.description;
            txtResult.Text = t.result;
        }
    }
}

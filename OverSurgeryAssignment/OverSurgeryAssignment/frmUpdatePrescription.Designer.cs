﻿namespace OverSurgeryAssignment
{
    partial class frmUpdatePrescription
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnExtend = new System.Windows.Forms.Button();
            this.dtpExtendPrescription = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // btnExtend
            // 
            this.btnExtend.Location = new System.Drawing.Point(273, 91);
            this.btnExtend.Name = "btnExtend";
            this.btnExtend.Size = new System.Drawing.Size(85, 35);
            this.btnExtend.TabIndex = 0;
            this.btnExtend.Text = "Extend";
            this.btnExtend.UseVisualStyleBackColor = true;
            this.btnExtend.Click += new System.EventHandler(this.btnExtend_Click);
            // 
            // dtpExtendPrescription
            // 
            this.dtpExtendPrescription.Location = new System.Drawing.Point(62, 98);
            this.dtpExtendPrescription.Name = "dtpExtendPrescription";
            this.dtpExtendPrescription.Size = new System.Drawing.Size(192, 20);
            this.dtpExtendPrescription.TabIndex = 1;
            // 
            // frmUpdatePrescription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 235);
            this.Controls.Add(this.dtpExtendPrescription);
            this.Controls.Add(this.btnExtend);
            this.Name = "frmUpdatePrescription";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Over Surgery Extend Prescription";
            this.Load += new System.EventHandler(this.frmUpdatePrescription_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnExtend;
        private System.Windows.Forms.DateTimePicker dtpExtendPrescription;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmUpdatePrescription : Form
    {
        private int? patientID;

        public frmUpdatePrescription(int? ID)
        {
            InitializeComponent();
            this.patientID = ID;
        }

        private void frmUpdatePrescription_Load(object sender, EventArgs e)
        {

        }

        private void btnExtend_Click(object sender, EventArgs e)
        {
            string date = dtpExtendPrescription.Value.ToString("yyyy-MM-dd");
            string selectString = Constants.updatePrescription;
            selectString = selectString.Replace("@ID", patientID.ToString());
            selectString = selectString.Replace("@date", date);


            DBConnection connection = DBConnection.getDBConnectionInstance();
            connection.openConnection();
            connection.updatePrescription(selectString);
            connection.closeConnection();
            MessageBox.Show("Extended!" ,"Prescription has been extended", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Hide();
        }
    }
}

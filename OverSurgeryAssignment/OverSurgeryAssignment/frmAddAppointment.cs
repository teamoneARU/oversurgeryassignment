﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmAddAppointment : Form
    {
        DataGridView view;
        Appointment appointment;

        public frmAddAppointment(DataGridView dgv, Appointment app)
        {
            InitializeComponent();
            view = dgv;
            appointment = app;
        }

        public frmAddAppointment() {
            InitializeComponent();
        }

        private void frmAddAppointment_Load(object sender, EventArgs e)
        {
            DBConnection connection = DBConnection.getDBConnectionInstance();

            DataSet sdDoctors = connection.getDataSet(Constants.getAllDoctors);
            DataTable dtDoctors = sdDoctors.Tables[0];

            cbDoctors.ValueMember = "StaffID";
            cbDoctors.DisplayMember = "DoctorNurse";
            cbDoctors.DataSource = dtDoctors;
            cbDoctors.SelectedIndex = -1;


           cbDoctors.Text = appointment.Doctor;

            DataSet sdPatients = connection.getDataSet(Constants.getAllPatients);
            DataTable dtPatients = sdPatients.Tables[0];

            cbPatients.ValueMember = "PatientID";
            cbPatients.DisplayMember = "PatientName";
            cbPatients.DataSource = dtPatients;
            cbPatients.SelectedIndex = -1;

            dtpDate.Value = appointment.Date;
            dtpStartTime.Value = appointment.StartTime;
            dtpEndTime.Value = appointment.EndTime;
        }


        private void btnAddAppointment_Click(object sender, EventArgs e)
        {
            DBConnection connection = DBConnection.getDBConnectionInstance();
            connection.openConnection();

            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            dictionary.Add("@patientID", (int) cbPatients.SelectedValue);
            dictionary.Add("@appointmentID", appointment.AppointmentID);

            connection.insertAppointment(Constants.addAppointment, dictionary);

            DataSet dg = connection.getDataSet(Constants.getAppointments);
            DataTable dt = dg.Tables[0];

            MessageBoxButtons buttons = MessageBoxButtons.OK;
            MessageBoxIcon icon = MessageBoxIcon.Information;
            string message = "Appointment Added";
            string caption = "";

            MessageBox.Show(message, caption, buttons, icon);
            this.Hide();

            view.DataSource = dt;
            view.Refresh();
            view.Update();
        }
    }
}

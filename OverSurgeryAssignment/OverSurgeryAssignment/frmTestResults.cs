﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmTestResults : Form
    {
        private int? patientID;
        DataTable dtTests;

        public frmTestResults(int? ID)
        {
            InitializeComponent();
            this.patientID = ID;
        }


        private void btnHome_Click(object sender, EventArgs e)
        {
            var home = new frmMain();
            home.Show();
            Hide();
        }


        private void frmTestResults_Load(object sender, EventArgs e)
        {
            string sql = Constants.getTests.Replace("@patientID", patientID.ToString());

            DataSet dsTests = DBConnection.getDBConnectionInstance().getDataSet(sql);

            dtTests = dsTests.Tables[0];

            dgvTests.DataSource = dtTests;

            dgvTests.Update();

            dgvTests.Refresh();

            dgvTests.ReadOnly = true;
        }

        private void frmTestResults_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int RowIndex = e.RowIndex;

            int testID = (int)dtTests.Rows[RowIndex]["TestID"];

            this.Hide();
            frmTestPrint a = new frmTestPrint(patientID);
            a.Show();
        }



        private void btnPrint_Click(object sender, EventArgs e)
        {

        }

        private void txtTest1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            var home = new frmPatientDetails();
            home.Show();
            Hide();
        }
    }
}

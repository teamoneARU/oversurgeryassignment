﻿namespace OverSurgeryAssignment
{
    partial class frmPatientDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFirstName = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblCounty = new System.Windows.Forms.Label();
            this.lblTownCity = new System.Windows.Forms.Label();
            this.lblAddress1 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.txtCounty = new System.Windows.Forms.TextBox();
            this.txtTownCity = new System.Windows.Forms.TextBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.lblPostcode = new System.Windows.Forms.Label();
            this.lblDoB = new System.Windows.Forms.Label();
            this.txtPostcode = new System.Windows.Forms.TextBox();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.lblGender = new System.Windows.Forms.Label();
            this.lblTelNo = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtGender = new System.Windows.Forms.TextBox();
            this.txtTelNo = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.btnEditPatient = new System.Windows.Forms.Button();
            this.btnViewAppts = new System.Windows.Forms.Button();
            this.btnViewTests = new System.Windows.Forms.Button();
            this.btnViewPrescriptions = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.lblPatientDetails = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.lblAddress2 = new System.Windows.Forms.Label();
            this.btnHome = new UI.button();
            this.SuspendLayout();
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(51, 98);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(57, 13);
            this.lblFirstName.TabIndex = 0;
            this.lblFirstName.Text = "First Name";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(60, 121);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(49, 13);
            this.lblSurname.TabIndex = 1;
            this.lblSurname.Text = "Surname";
            // 
            // lblCounty
            // 
            this.lblCounty.AutoSize = true;
            this.lblCounty.Location = new System.Drawing.Point(68, 225);
            this.lblCounty.Name = "lblCounty";
            this.lblCounty.Size = new System.Drawing.Size(40, 13);
            this.lblCounty.TabIndex = 5;
            this.lblCounty.Text = "County";
            // 
            // lblTownCity
            // 
            this.lblTownCity.AutoSize = true;
            this.lblTownCity.Location = new System.Drawing.Point(53, 199);
            this.lblTownCity.Name = "lblTownCity";
            this.lblTownCity.Size = new System.Drawing.Size(56, 13);
            this.lblTownCity.TabIndex = 7;
            this.lblTownCity.Text = "Town/City";
            // 
            // lblAddress1
            // 
            this.lblAddress1.AutoSize = true;
            this.lblAddress1.Location = new System.Drawing.Point(32, 147);
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Size = new System.Drawing.Size(77, 13);
            this.lblAddress1.TabIndex = 9;
            this.lblAddress1.Text = "Address Line 1";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(126, 95);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.ReadOnly = true;
            this.txtFirstName.Size = new System.Drawing.Size(142, 20);
            this.txtFirstName.TabIndex = 2;
            // 
            // txtCounty
            // 
            this.txtCounty.Location = new System.Drawing.Point(126, 222);
            this.txtCounty.Name = "txtCounty";
            this.txtCounty.ReadOnly = true;
            this.txtCounty.Size = new System.Drawing.Size(142, 20);
            this.txtCounty.TabIndex = 7;
            // 
            // txtTownCity
            // 
            this.txtTownCity.Location = new System.Drawing.Point(126, 196);
            this.txtTownCity.Name = "txtTownCity";
            this.txtTownCity.ReadOnly = true;
            this.txtTownCity.Size = new System.Drawing.Size(142, 20);
            this.txtTownCity.TabIndex = 6;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(126, 144);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.ReadOnly = true;
            this.txtAddress1.Size = new System.Drawing.Size(142, 20);
            this.txtAddress1.TabIndex = 4;
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(126, 118);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.ReadOnly = true;
            this.txtSurname.Size = new System.Drawing.Size(142, 20);
            this.txtSurname.TabIndex = 3;
            // 
            // lblPostcode
            // 
            this.lblPostcode.AutoSize = true;
            this.lblPostcode.Location = new System.Drawing.Point(56, 251);
            this.lblPostcode.Name = "lblPostcode";
            this.lblPostcode.Size = new System.Drawing.Size(52, 13);
            this.lblPostcode.TabIndex = 19;
            this.lblPostcode.Text = "Postcode";
            // 
            // lblDoB
            // 
            this.lblDoB.AutoSize = true;
            this.lblDoB.Location = new System.Drawing.Point(42, 277);
            this.lblDoB.Name = "lblDoB";
            this.lblDoB.Size = new System.Drawing.Size(66, 13);
            this.lblDoB.TabIndex = 17;
            this.lblDoB.Text = "Date of Birth";
            // 
            // txtPostcode
            // 
            this.txtPostcode.Location = new System.Drawing.Point(126, 248);
            this.txtPostcode.Name = "txtPostcode";
            this.txtPostcode.ReadOnly = true;
            this.txtPostcode.Size = new System.Drawing.Size(142, 20);
            this.txtPostcode.TabIndex = 8;
            // 
            // dtpDOB
            // 
            this.dtpDOB.Location = new System.Drawing.Point(126, 274);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(142, 20);
            this.dtpDOB.TabIndex = 9;
            // 
            // lblGender
            // 
            this.lblGender.AutoSize = true;
            this.lblGender.Location = new System.Drawing.Point(66, 303);
            this.lblGender.Name = "lblGender";
            this.lblGender.Size = new System.Drawing.Size(42, 13);
            this.lblGender.TabIndex = 24;
            this.lblGender.Text = "Gender";
            // 
            // lblTelNo
            // 
            this.lblTelNo.AutoSize = true;
            this.lblTelNo.Location = new System.Drawing.Point(70, 355);
            this.lblTelNo.Name = "lblTelNo";
            this.lblTelNo.Size = new System.Drawing.Size(42, 13);
            this.lblTelNo.TabIndex = 25;
            this.lblTelNo.Text = "Tel No.";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(76, 329);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(32, 13);
            this.lblEmail.TabIndex = 27;
            this.lblEmail.Text = "Email";
            // 
            // txtGender
            // 
            this.txtGender.Location = new System.Drawing.Point(126, 300);
            this.txtGender.Name = "txtGender";
            this.txtGender.ReadOnly = true;
            this.txtGender.Size = new System.Drawing.Size(142, 20);
            this.txtGender.TabIndex = 10;
            // 
            // txtTelNo
            // 
            this.txtTelNo.Location = new System.Drawing.Point(126, 352);
            this.txtTelNo.Name = "txtTelNo";
            this.txtTelNo.ReadOnly = true;
            this.txtTelNo.Size = new System.Drawing.Size(142, 20);
            this.txtTelNo.TabIndex = 12;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(126, 326);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = true;
            this.txtEmail.Size = new System.Drawing.Size(142, 20);
            this.txtEmail.TabIndex = 11;
            // 
            // btnEditPatient
            // 
            this.btnEditPatient.Location = new System.Drawing.Point(301, 69);
            this.btnEditPatient.Name = "btnEditPatient";
            this.btnEditPatient.Size = new System.Drawing.Size(97, 42);
            this.btnEditPatient.TabIndex = 16;
            this.btnEditPatient.Text = "Edit Patient Details";
            this.btnEditPatient.UseVisualStyleBackColor = true;
            this.btnEditPatient.Click += new System.EventHandler(this.btnEditPatient_Click);
            // 
            // btnViewAppts
            // 
            this.btnViewAppts.Location = new System.Drawing.Point(71, 378);
            this.btnViewAppts.Name = "btnViewAppts";
            this.btnViewAppts.Size = new System.Drawing.Size(123, 31);
            this.btnViewAppts.TabIndex = 13;
            this.btnViewAppts.Text = "View Appointments";
            this.btnViewAppts.UseVisualStyleBackColor = true;
            this.btnViewAppts.Click += new System.EventHandler(this.btnViewAppts_Click_1);
            // 
            // btnViewTests
            // 
            this.btnViewTests.Location = new System.Drawing.Point(71, 452);
            this.btnViewTests.Name = "btnViewTests";
            this.btnViewTests.Size = new System.Drawing.Size(123, 31);
            this.btnViewTests.TabIndex = 15;
            this.btnViewTests.Text = "View Test Results";
            this.btnViewTests.UseVisualStyleBackColor = true;
            this.btnViewTests.Click += new System.EventHandler(this.btnViewTests_Click_1);
            // 
            // btnViewPrescriptions
            // 
            this.btnViewPrescriptions.Location = new System.Drawing.Point(71, 415);
            this.btnViewPrescriptions.Name = "btnViewPrescriptions";
            this.btnViewPrescriptions.Size = new System.Drawing.Size(123, 31);
            this.btnViewPrescriptions.TabIndex = 14;
            this.btnViewPrescriptions.Text = "View Prescriptions";
            this.btnViewPrescriptions.UseVisualStyleBackColor = true;
            this.btnViewPrescriptions.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(301, 117);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(97, 42);
            this.btnSave.TabIndex = 17;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(126, 69);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(142, 20);
            this.txtID.TabIndex = 36;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(90, 72);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(18, 13);
            this.lblID.TabIndex = 37;
            this.lblID.Text = "ID";
            this.lblID.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // lblPatientDetails
            // 
            this.lblPatientDetails.AutoSize = true;
            this.lblPatientDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPatientDetails.Location = new System.Drawing.Point(28, 18);
            this.lblPatientDetails.Name = "lblPatientDetails";
            this.lblPatientDetails.Size = new System.Drawing.Size(256, 39);
            this.lblPatientDetails.TabIndex = 38;
            this.lblPatientDetails.Text = "Patient Details";
            this.lblPatientDetails.UseMnemonic = false;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(126, 170);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.ReadOnly = true;
            this.txtAddress2.Size = new System.Drawing.Size(142, 20);
            this.txtAddress2.TabIndex = 5;
            // 
            // lblAddress2
            // 
            this.lblAddress2.AutoSize = true;
            this.lblAddress2.Location = new System.Drawing.Point(32, 173);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(77, 13);
            this.lblAddress2.TabIndex = 39;
            this.lblAddress2.Text = "Address Line 2";
            // 
            // btnHome
            // 
            this.btnHome.flatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.btnHome.Location = new System.Drawing.Point(301, 165);
            this.btnHome.Name = "btnHome";
            this.btnHome.OpenForm = null;
            this.btnHome.Size = new System.Drawing.Size(97, 42);
            this.btnHome.TabIndex = 40;
            this.btnHome.Text = "Home";
            // 
            // frmPatientDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 519);
            this.Controls.Add(this.btnHome);
            this.Controls.Add(this.txtAddress2);
            this.Controls.Add(this.lblAddress2);
            this.Controls.Add(this.lblPatientDetails);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnViewPrescriptions);
            this.Controls.Add(this.btnViewTests);
            this.Controls.Add(this.btnViewAppts);
            this.Controls.Add(this.btnEditPatient);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtTelNo);
            this.Controls.Add(this.txtGender);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblTelNo);
            this.Controls.Add(this.lblGender);
            this.Controls.Add(this.dtpDOB);
            this.Controls.Add(this.txtPostcode);
            this.Controls.Add(this.lblPostcode);
            this.Controls.Add(this.lblDoB);
            this.Controls.Add(this.txtSurname);
            this.Controls.Add(this.txtAddress1);
            this.Controls.Add(this.txtTownCity);
            this.Controls.Add(this.txtCounty);
            this.Controls.Add(this.txtFirstName);
            this.Controls.Add(this.lblAddress1);
            this.Controls.Add(this.lblTownCity);
            this.Controls.Add(this.lblCounty);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.lblFirstName);
            this.Name = "frmPatientDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Over Surgery Patient Details";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPatientDetails_FormClosing);
            this.Load += new System.EventHandler(this.frmPatient_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblCounty;
        private System.Windows.Forms.Label lblTownCity;
        private System.Windows.Forms.Label lblAddress1;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.TextBox txtCounty;
        private System.Windows.Forms.TextBox txtTownCity;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label lblPostcode;
        private System.Windows.Forms.Label lblDoB;
        private System.Windows.Forms.TextBox txtPostcode;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.Label lblGender;
        private System.Windows.Forms.Label lblTelNo;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtGender;
        private System.Windows.Forms.TextBox txtTelNo;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Button btnEditPatient;
        private System.Windows.Forms.Button btnViewAppts;
        private System.Windows.Forms.Button btnViewTests;
        private System.Windows.Forms.Button btnViewPrescriptions;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblPatientDetails;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label lblAddress2;
        private UI.button btnHome;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverSurgeryAssignment
{
    class Constants
    {
        public static String getUserDetails = "SELECT * FROM Users WHERE Username = @username COLLATE SQL_Latin1_General_CP1_CS_AS;";
        public static String getPatientDetails = "SELECT * FROM Patient";

        //Query to update patient details from frmPatientDetails
        public static String updatePatient = "UPDATE Patient SET FirstName = @firstName, LastName = @lastName, Gender = @gender, DateOfBirth = @dob, AddressLine1 = @Add1, AddressLine2 = @Add2, Town = @town, Postcode = @postcode, Telephone = @tel, Email = @email WHERE PatientID = @id;";


        // query for the grid view for frmDailyAppointments
        public static String getDailyAppointments =
            "SELECT " +
            "CONCAT (S.FirstName, ' ', S.LastName) AS Doctor_Nurse, CONCAT (P.FirstName, ' ', P.LastName) AS Patient, A.Date, A.StartTime, A.EndTime, A.Reason " +
            "FROM Appointment A, Patient P, Staff S " +
            "WHERE A.PatientID = P.PatientID AND A.StaffID = S.StaffID AND A.Date = @Date" +
            " order by S.LastName, S.FirstName, A.StartTime";


        //query for grid view for frmStaffSchedule
        public static String getStaffSch =
              "SELECT " +
            "CONCAT (S.FirstName, ' ', S.LastName) AS Staff_Member, W.StartTime, W.EndTime " +
            "FROM Staff S, StaffSchedule W " +
            "WHERE S.StaffID = W.StaffID AND W.Date = @Date" +
            " order by S.LastName";

        // public static String getStaffSch =" CONCAT(S.FirstName, ' ', S.LastName) AS Staff_Member, W.StartTime, W.EndTime FROM staff S, staffschedule W WHERE Date = '2017-11-15' AND S.StaffID = W.StaffID";

        //query for the combo box in frmDailyAppointments to add doctors and nurses names
        public static String getDocsNurse = "SELECT StaffID, CONCAT(FirstName, ' ', LastName) AS Doctor_Nurse FROM Staff WHERE StaffTypeID = 1 OR StaffTypeID = 4";
        public static String getDocAppointments =
            "SELECT " +
            "CONCAT (S.FirstName, ' ', S.LastName) AS Doctor_Nurse, CONCAT (P.FirstName, ' ', P.LastName) AS Patient, A.Date, A.StartTime, A.EndTime, A.Reason " +
            "FROM Appointment A, Patient P, Staff S " +
            "WHERE A.PatientID = P.PatientID AND A.StaffID = S.StaffID AND S.LastName = '@DocNurse' AND A.Date = @Date" +
            " order by S.LastName, S.FirstName, A.StartTime";

        public static String getDoctors = "SELECT CONCAT(Staff.FirstName, ' ', Staff.LastName) AS DoctorNurse " +
            "FROM Appointment " +
            "INNER JOIN Patient ON Appointment.PatientID = Patient.PatientID " +
            "INNER JOIN Staff ON Appointment.StaffID = Staff.StaffID " +
            "GROUP BY CONCAT(Staff.FirstName, ' ', Staff.LastName)";

        public static String getAllDoctors = "SELECT Staff.StaffID, CONCAT(Staff.FirstName, ' ', Staff.LastName) AS DoctorNurse FROM Staff;";

        public static String getAllPatients = "SELECT Patient.PatientID, CONCAT(Patient.FirstName, ' ', Patient.LastName) AS PatientName FROM Patient;";

        public static String getAppointments =
            "SELECT Appointment.AppointmentID, Appointment.PatientID, Appointment.Date, Appointment.StartTime, Appointment.EndTime, Appointment.Reason, CONCAT(Staff.FirstName, ' ', Staff.LastName) AS DoctorNurse FROM Appointment INNER JOIN Staff ON Appointment.StaffID = Staff.StaffID WHERE Appointment.PatientID IS NULL;";

        public static String addAppointment = "UPDATE Appointment " +
            "SET PatientID = @patientID " +
            "WHERE AppointmentID = @appointmentID;";

        //get test results for the print form
        public static String getTestPrint = "SELECT testResult, testDescription FROM Tests WHERE testID=@TestNo";

        //get Users which are managers for Create User form
        public static String getManager = "SELECT U.Username, U.Password FROM Users U, Staff S " +
            "WHERE S.StaffTypeID = 2 AND S.UserId = U.UserID AND U.Username = '@ManagerUsername' AND U.Password = '@ManagerPassword'";

        // get Users for create user form
        public static String getUserCreate = "SELECT * FROM Users WHERE Username = '@username';";

        // insert new users into database
        public static String insertUser = "INSERT into Users (Username, Password) VALUES (@Username, @Password) ";

        public static String getTests= "SELECT * FROM Tests WHERE patientID = @patientID";

        public static string addPatient = "INSERT INTO Patient (FirstName, LastName, Gender, DateOfBirth, AddressLine1, AddressLine2, Town, Postcode, Telephone, Email) " +
            "VALUES(@fname, @lname, @gender, @dob, @line1, @line2, @city, @postcode, @telephone, @email);";

        //query to get prescription
        public static string getPrescription = "SELECT PrescriptionID, Medicine, StartDate, EndDate, ExtendedDate FROM Prescription WHERE PatientID = @patientID";

        //Query to extend prescription
        public static string updatePrescription = "UPDATE Prescription SET ExtendedDate = '@date' WHERE PatientID = @ID";

        public static String getPatientTests = "SELECT testID, testDescription, testResult FROM Tests WHERE patientID = @id";

        //Query to check extandable
        public static String getPrescriptionDetails = "SELECT * FROM Prescription WHERE PrescriptionID = @id";
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmAddPatient : Form
    {
        Patient patient = null;
        public frmAddPatient(Patient p)
        {
            InitializeComponent();

            patient = p;
        }

        public frmAddPatient() {
            InitializeComponent();
        }

        private void frmAddPatient_Load(object sender, EventArgs e)
        {
            txtFirstName.Text = patient.FirstName;
            txtSurname.Text = patient.LastName;
            txtAddress1.Text = patient.Addressline1;
            txtAddress2.Text = patient.Addressline2;
            txtTownCity.Text = patient.City;
            txtPostcode.Text = patient.Postcode;
            dtpDOB.Value = (patient.DOB.HasValue) ? patient.DOB.Value : DateTime.Now ;
            txtEmail.Text = patient.Email;
            txtTelNo.Text = patient.Telephone;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("@fname", txtFirstName.Text);
            parameters.Add("@lname", txtSurname.Text);
            parameters.Add("@gender", cbGender.Text);
            parameters.Add("@dob", dtpDOB.Value.ToString("yyyy-MM-dd"));
            parameters.Add("@line1", txtAddress1.Text);
            parameters.Add("@line2", txtAddress2.Text);
            parameters.Add("@city", txtTownCity.Text);
            parameters.Add("@postcode", txtPostcode.Text);
            parameters.Add("@telephone", txtTelNo.Text);
            parameters.Add("@email", txtEmail.Text);

            DBConnection connection = DBConnection.getDBConnectionInstance();

            connection.openConnection();
            int rows = connection.insertNewPatient(Constants.addPatient, parameters);
            connection.closeConnection();

            if (rows == 1) {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBoxIcon icon = MessageBoxIcon.Information;
                string Message = "Patient Successfully created";

                MessageBox.Show(Message, "", buttons, icon);
            }

        }

        private void frmAddPatient_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
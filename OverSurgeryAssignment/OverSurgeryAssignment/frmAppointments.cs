﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Globalization;

namespace OverSurgeryAssignment
{
    public partial class frmAppointments : Form
    {
        List<Appointment> appointments;
        Appointment app;
        DataTable dt2;

        public frmAppointments()
        {
            InitializeComponent();
        }

        private void frmAppointments_Load(object sender, EventArgs e)
        {

            DBConnection connection = DBConnection.getDBConnectionInstance();
            DataSet doctors = connection.getDataSet(Constants.getDoctors);

            DataTable dt = doctors.Tables[0];
           
            cmbDoctors.ValueMember = "DoctorNurse";
            cmbDoctors.DisplayMember = "DoctorNurse";
           
            cmbDoctors.DataSource = dt;
            cmbDoctors.SelectedIndex = -1;

            DataSet a = connection.getDataSet(Constants.getAppointments);
            dt2 = a.Tables[0];

            dgvAppointments.DataSource = dt2;
            dgvAppointments.Update();
            dgvAppointments.Refresh();
            dgvAppointments.AutoResizeColumns();

            appointments = new List<Appointment>();

            foreach (DataRow row in dt2.Rows) {
                DateTime startTime = DateTime.Parse(row["StartTime"].ToString());
                DateTime endTime = DateTime.Parse(row["EndTime"].ToString());
                DateTime date = DateTime.Parse(row["Date"].ToString());

                appointments.Add(new Appointment() {
                    StartTime = startTime,
                    EndTime = endTime,
                    Date = date
                });
            }

            dgvAppointments.ReadOnly = true;
        }

        private void btnNewAppointment_Click(object sender, EventArgs e)
        {
            frmAddAppointment addAppointmentform = new frmAddAppointment(dgvAppointments, app);

            addAppointmentform.Show();
        }

        private void dgvAppointments_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int RowIndex = e.RowIndex;

            int appointmentID = (int)dt2.Rows[RowIndex]["AppointmentID"];
            string doctor = dt2.Rows[RowIndex]["DoctorNurse"].ToString();
            DateTime startTime = DateTime.Parse(dt2.Rows[RowIndex]["StartTime"].ToString());
            DateTime endTime = DateTime.Parse(dt2.Rows[RowIndex]["EndTime"].ToString());
            DateTime date = DateTime.Parse(dt2.Rows[RowIndex]["Date"].ToString());

            app = new Appointment {
                AppointmentID = appointmentID,
                Date = date,
                StartTime = startTime,
                EndTime = endTime,
                Doctor = doctor
            };

            frmAddAppointment addAppointmentform = new frmAddAppointment(dgvAppointments, app);
            addAppointmentform.Show();
        }

        private void frmAppointments_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            frmPatientDetails patientDetails = new frmPatientDetails();
            patientDetails.Show();
            this.Hide();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            frmMain main = new frmMain();
            main.Show();
            this.Hide();
        }
    }
}

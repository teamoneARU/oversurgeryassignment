﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmPatientDetails : Form
    {
        Patient patient;

        public frmPatientDetails()
        {
            InitializeComponent();
        }

        public frmPatientDetails(Patient p)
        {
            InitializeComponent();
            patient = p;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            var prescriptions = new frmPrescription(patient.PatientID);
            prescriptions.Show();
            Hide();
        }

        private void btnEditPatient_Click(object sender, EventArgs e)
        {
            //Allowing user to change textbox fields
            txtID.ReadOnly = false;
            txtFirstName.ReadOnly = false;
            txtSurname.ReadOnly = false;
            txtAddress1.ReadOnly = false;
            txtAddress2.ReadOnly = false;
            txtTownCity.ReadOnly = false;
            txtCounty.ReadOnly = false;
            txtPostcode.ReadOnly = false;
            txtGender.ReadOnly = false;
            txtEmail.ReadOnly = false;
            txtTelNo.ReadOnly = false;
            btnSave.Visible = true;
            btnEditPatient.Visible = false;

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //Disabling editing on textboxes to prevent accidental changes
            txtFirstName.ReadOnly = true;
            txtSurname.ReadOnly = true;
            txtAddress1.ReadOnly = true;
            txtAddress2.ReadOnly = true;
            txtTownCity.ReadOnly = true;
            txtCounty.ReadOnly = true;
            txtPostcode.ReadOnly = true;
            txtGender.ReadOnly = true;
            txtEmail.ReadOnly = true;
            txtTelNo.ReadOnly = true;
            btnSave.Visible = false;
            btnEditPatient.Visible = true;


            //Update record in database
            DBConnection connection = DBConnection.getDBConnectionInstance();

            // Open the connection
            connection.openConnection();

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("@firstName", txtFirstName.Text);
            parameters.Add("@lastName", txtSurname.Text);
            parameters.Add("@gender", txtGender.Text);
            parameters.Add("@dob", dtpDOB.Value.ToString("yyyy-MM-dd"));
            parameters.Add("@Add1", txtAddress1.Text);
            parameters.Add("@Add2", txtAddress2.Text);
            parameters.Add("@town", txtTownCity.Text);
            parameters.Add("@postcode", txtPostcode.Text);
            parameters.Add("@tel", txtTelNo.Text);
            parameters.Add("@email", txtEmail.Text);
            parameters.Add("@id", txtID.Text);

            // Get the data from the database as SqlDataReader
            connection.updatePatient(Constants.updatePatient, parameters);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var home = new frmMain();
            home.Show();
            Hide();
        }

        private void frmPatient_Load(object sender, EventArgs e)
        {
            btnHome.OpenForm = new frmMain();

            //Displaying patient data in the textboxes
            if (patient != null) {
                txtFirstName.Text = patient.FirstName;
                txtSurname.Text = patient.LastName;
                txtAddress1.Text = patient.Addressline1;
                txtTownCity.Text = patient.City;
                txtPostcode.Text = patient.Postcode;
                txtID.Text = patient.PatientID.ToString();
                dtpDOB.Value = (DateTime) patient.DOB;
                txtAddress2.Text = patient.Addressline2;
                txtGender.Text = patient.Gender1;
                txtTelNo.Text = patient.Telephone;
                txtEmail.Text = patient.Email;
                
            }

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void frmPatientDetails_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {


        }

        private void btnViewTests_Click_1(object sender, EventArgs e)
        {
            var viewTests = new frmTestResults(patient.PatientID);
            viewTests.Show();
            Hide();
        }

        private void btnViewAppts_Click_1(object sender, EventArgs e)
        {
            var viewAppts = new frmAppointments();
            viewAppts.Show();
            Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmStaffSchedule : Form
    {
        public frmStaffSchedule()
        {
            InitializeComponent();
        }

        private void date_changed(object sender, EventArgs e)
        {
            //To enable date change from SQL as was in the wrong format
            String dateString = "'" + dtpStaffSchedule.Value.ToString("yyyy-MM-dd") + "'";

            String selectString = Constants.getStaffSch.Replace("@Date", dateString);

            //logging
            core.Logger.Instance.Log("frmStaffSchedule: dtpStaffSchedule - Date Changed");

            DataSet dsStaffSch = DBConnection.getDBConnectionInstance().getDataSet(selectString);

            DataTable dtStaffSch = dsStaffSch.Tables[0];

            dgvStaffSch.DataSource = dtStaffSch;

            dgvStaffSch.Update();

            dgvStaffSch.Refresh();

        }

        // take back to the Main Menu
        private void btnHome_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMain mainForm = new frmMain();
            mainForm.Show();
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmCreateUser : Form
    {
        public frmCreateUser()
        {
            InitializeComponent();
        }

        private void btnCreateUser_Click(object sender, EventArgs e)
        {
            //need to check manager before the insertion can run
            String managerUsername = txtManagerUsername.Text;
            String managerPassword = txtManagerPassword.Text;

            String selectString = Constants.getManager;
            selectString = selectString.Replace("@ManagerUsername", managerUsername);
            selectString = selectString.Replace("@ManagerPassword", managerPassword);

            //logging
            core.Logger.Instance.Log("frmCreateUser: btnCreateUser_Click - Checking if valid manager " + managerUsername);
            
            //checking if valid manager
            DataSet dsManager = DBConnection.getDBConnectionInstance().getDataSet(selectString);
            DataTable dtManager = dsManager.Tables[0];

            //need to check User 
            String user = txtUserName.Text;
            selectString = Constants.getUserCreate;
            selectString = selectString.Replace("@username", user);


            //logging
            core.Logger.Instance.Log("frmCreateUser: btnCreateUser_Click - Checking users already exists " + user);

            DataSet dsUser = DBConnection.getDBConnectionInstance().getDataSet(selectString);
            DataTable dtUser = dsUser.Tables[0];

            //check the textboxes are not blank
            if (txtUserName.Text == "" || txtUserPassword.Text == "")
            {
                MessageBox.Show("Error - Please enter details again");

                
            }

            else
            {
                //if password correct then
                if (dtManager.Rows.Count > 0)
                {
                    // check user not already set up
                    if (dtUser.Rows.Count == 0)
                    {
                        //insert into the database the username and password from the text boxes
                        DBConnection.getDBConnectionInstance().insertNewUser(Constants.insertUser, txtUserName.Text, txtUserPassword.Text);

                        //logging
                        core.Logger.Instance.Log("frmCreateUser: btnCreateUser_Click - inserting user into database " + user);


                        //add pop up message to confirm created and shut screen and take back to main screen
                        MessageBox.Show("New user created.");

                        //clear text boxes on new user setup  txtDocNurse.Text = String.Empty;
                        txtUserName.Text = String.Empty;
                        txtUserPassword.Text = String.Empty;
                        txtManagerUsername.Text = String.Empty;
                        txtManagerPassword.Text = String.Empty;
                    }
                    else
                    {
                        MessageBox.Show("Error - User name already taken");
                    }

                }
                else
                {
                    MessageBox.Show("Manager details not valid - Please enter details again");
                }
            }
        }

        // take back to the Main Menu
        private void btnHome_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMain mainForm = new frmMain();
            mainForm.Show();

            //logging
            core.Logger.Instance.Log("frmCreateUser: btnHome_Click");

        }

        private void txtUserPassword_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

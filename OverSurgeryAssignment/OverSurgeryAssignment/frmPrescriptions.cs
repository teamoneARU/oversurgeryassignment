﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    public partial class frmPrescription : Form
    {
        private int? patientID;
        DataTable dtPrescriptions;
        

        public frmPrescription(int? patientID)
        {
            InitializeComponent();
            this.patientID = patientID;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            var home = new frmMain();
            home.Show();
            Hide();
        }

        private void frmPrescriptions_Load(object sender, EventArgs e)
        {
            string sql = Constants.getPrescription.Replace("@patientID", patientID.ToString());

            DataSet dsPrescriptions = DBConnection.getDBConnectionInstance().getDataSet(sql);

            dtPrescriptions = dsPrescriptions.Tables[0];

            dgvPrescriptions.DataSource = dtPrescriptions;

            dgvPrescriptions.Update();

            dgvPrescriptions.Refresh();
        }

        private void frmPrescriptions_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int RowIndex = e.RowIndex;

            int prescriptionID = (int)dtPrescriptions.Rows[RowIndex]["PrescriptionID"];
            
            DBConnection connection = DBConnection.getDBConnectionInstance();
            connection.openConnection();


            if (connection.isPrescriptionExtendable(Constants.getPrescriptionDetails, prescriptionID))
            {
                frmUpdatePrescription a = new frmUpdatePrescription(patientID);
                a.Show();
            }
            else
            {
                MessageBox.Show("This prescription is not extendable", "Not Extendable!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            var back = new frmPatientDetails();
            back.Show();
            Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverSurgeryAssignment
{
    public class Patient
    {
        int? patientID;
        String firstName;
        String lastName;
        String addressline1;
        String addressline2;
        String city;
        String postcode;
        DateTime? dob;
        String Gender;
        String telephone;
        String email;

        public int? PatientID { get => patientID; set => patientID = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Addressline1 { get => addressline1; set => addressline1 = value; }
        public string Addressline2 { get => addressline2; set => addressline2 = value; }
        public string City { get => city; set => city = value; }
        public string Postcode { get => postcode; set => postcode = value; }
        public DateTime? DOB { get => dob; set => dob = value; }
        public string Gender1 { get => Gender; set => Gender = value; }
        public string Telephone { get => telephone; set => telephone = value; }
        public string Email { get => email; set => email = value; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverSurgeryAssignment
{
    public class Appointment
    {
        int patientID;
        int appointmentID;
        string doctor;
        DateTime startTime;
        DateTime endTime;
        DateTime date;

        public int PatientID { get => patientID; set => patientID = value; }
        public int AppointmentID { get => appointmentID; set => appointmentID = value; }
        public string Doctor { get => doctor; set => doctor = value; }
        public DateTime StartTime { get => startTime; set => startTime = value; }
        public DateTime EndTime { get => endTime; set => endTime = value; }
        public DateTime Date { get => date; set => date = value; }

    }
}
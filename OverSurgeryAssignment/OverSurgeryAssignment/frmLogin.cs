﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OverSurgeryAssignment;

namespace OverSurgeryAssignment
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (validateUser())
            {
                core.Utils.openWindow(this, new frmMain());
            }
            else
            {
                lblErrorMessage.Visible = true;
                this.Height = 200;
                lblUsername.Left = 46;
                lblUsername.Top = 52;
                txtUsername.Left = 132;
                txtUsername.Top = 53;

                lblPassword.Left = 46;
                lblPassword.Top = 83;
                txtPassword.Left = 131;
                txtPassword.Top = 84;

                btnLogin.Left = 146;
                btnLogin.Top = 117;
            }
        }

        public bool validateUser()
        {
            String username = txtUsername.Text.Trim();
            String password = txtPassword.Text.Trim();

            // Return the database connection from DBConnection.
            DBConnection connection = DBConnection.getDBConnectionInstance();

            // Open the connection
            connection.openConnection();

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("@username", username);

            // Get the data from the database as SqlDataReader
            SqlDataReader reader = connection.getRecord(Constants.getUserDetails, parameters);

            DataTable dt = new DataTable();
            dt.Load(reader);

            // get the number of rows
            int rowCount = dt.Select().Length;

            // check if the number of rows is 0 or 1
            if (rowCount != 0) {
                // get the password from the row retrieved from the database
                String DBPassword = dt.Rows[0]["Password"].ToString().Trim();

                // compare the password from the database and the password entered.
                if (DBPassword.Equals(password)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            this.Height = 179;
            this.StartPosition = FormStartPosition.CenterScreen;
            txtPassword.PasswordChar = '\u2022';

            lblUsername.Left = 46;
            lblUsername.Top = 35;
            txtUsername.Left = 132;
            txtUsername.Top = 36;

            lblPassword.Left = 46;
            lblPassword.Top = 66;
            txtPassword.Left = 131;
            txtPassword.Top = 67;

            btnLogin.Left = 146;
            btnLogin.Top = 100;
        }

        private void txt_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                btnLogin.PerformClick();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment {
    public partial class frmPatientFind : Form {
        Patient patient = null;
        Boolean dateSelected = false;

        public frmPatientFind() {
            InitializeComponent();
        }

        private void frmPatientFind_Load(object sender, EventArgs e) {
            txtID.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtAddressLine1.Text = "";
            txtAddressLine2.Text = "";
            txtCity.Text = "";
            txtPostcode.Text = "";
        }

        private void btnNew_Click(object sender, EventArgs e) {
            Patient p = new Patient();
            getData(p);

            core.Logger.Instance.Log("asdfasdf");

            if (!checkPatientExists(p)) {
                string messageboxText = "This patient doesn't exist. \nDo you want to add a new patient.";
                string caption = "New Patient";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                MessageBoxIcon icon = MessageBoxIcon.Question;

                DialogResult results = MessageBox.Show(messageboxText, caption, buttons, icon);

                switch (results) {
                    case DialogResult.Yes:
                        //core.Utils.openWindow(this, new frmAddPatient(p));

                        frmAddPatient addPatient = new frmAddPatient(p);
                        this.Close();
                        addPatient.Show();
                        break;
                    case DialogResult.No:
                        break;
                }
            } else {
                string messageboxText = "This patient already exists.";
                string caption = "New patient";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBoxIcon icon = MessageBoxIcon.Error;

                MessageBox.Show(messageboxText, caption, buttons, icon);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e) {
            Patient p = new Patient();
            getData(p);

            if (checkPatientExists(p)) {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                MessageBoxIcon icon = MessageBoxIcon.Information;
                string messageboxText = "Patient exists, do you \nwant to view their detials";
                string caption = "Patient details";

                DialogResult result = MessageBox.Show(messageboxText, caption, buttons, icon);

                switch (result) {
                    case DialogResult.Yes:
                        getPatientDetails(p);
                        core.Utils.openWindow(this, new frmPatientDetails(patient));

                        break;
                    case DialogResult.No:
                        break;
                }
            } else {
                MessageBox.Show("Patient doesn't exist");
           }
        }

        private SqlDataReader getPatientDetails(Patient p)
        {
            DBConnection connection = DBConnection.getDBConnectionInstance();
            connection.openConnection();
            SqlDataReader reader = connection.getPatient(Constants.getPatientDetails, p);


            DataTable dt = new DataTable();
            dt.Load(reader);

            patient = new Patient();

            patient.PatientID = (int?) dt.Rows[0]["patientID"];
            patient.FirstName = dt.Rows[0]["FirstName"].ToString();
            patient.LastName = dt.Rows[0]["LastName"].ToString();
            patient.Gender1 = dt.Rows[0]["Gender"].ToString();
            patient.DOB = (DateTime?)dt.Rows[0]["DateOfBirth"];
            patient.Addressline1 = dt.Rows[0]["AddressLine1"].ToString();
            patient.Addressline2 = dt.Rows[0]["AddressLine2"].ToString();
            patient.City = dt.Rows[0]["Town"].ToString();
            patient.Postcode = dt.Rows[0]["Postcode"].ToString();
            patient.Telephone = dt.Rows[0]["Telephone"].ToString();
            patient.Email = dt.Rows[0]["Email"].ToString();

            connection.closeConnection();

            return reader;
        }

        private Patient getData(Patient patient) {
            patient.PatientID = (string.IsNullOrEmpty(txtID.Text)) ? null : (int?) Int32.Parse(txtID.Text);
            patient.FirstName = (string.IsNullOrWhiteSpace(txtFirstName.Text)) ? null : txtFirstName.Text;
            patient.LastName = (string.IsNullOrWhiteSpace(txtLastName.Text)) ? null : txtLastName.Text; ;
            patient.Addressline1 = (string.IsNullOrWhiteSpace(txtAddressLine1.Text)) ? null : txtAddressLine1.Text;
            patient.Addressline2 = (string.IsNullOrWhiteSpace(txtAddressLine2.Text)) ? null : txtAddressLine2.Text;
            patient.City = (string.IsNullOrWhiteSpace(txtCity.Text)) ? null : txtCity.Text;
            patient.Postcode = (string.IsNullOrWhiteSpace(txtPostcode.Text)) ? null : txtPostcode.Text;
            patient.DOB = (dateSelected) ? dtpDOB.Value : (DateTime?) null;

            return patient;
        }

        private bool checkPatientExists(Patient p) {
            bool patientExists = false;

            DBConnection connection = DBConnection.getDBConnectionInstance();
            connection.openConnection();

            SqlDataReader reader = connection.getPatient(Constants.getPatientDetails, p);

            DataTable dt = new DataTable();
            dt.Load(reader);

            // get the number of rows
            int rowCount = dt.Select().Length;

            // check if the number of rows is 0 or 1
            if (rowCount != 0) {
                patientExists = true;

                // get the password from the row retrieved from the database
                String DateOfBirth = dt.Rows[0]["DateOfBirth"].ToString().Trim();
            } 
            connection.closeConnection();

            return patientExists;
        }

        private void dtpDOB_ValueChanged(object sender, EventArgs e)
        {
            dateSelected = true;
        }

        private void lblReset_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtAddressLine1.Text = "";
            txtAddressLine2.Text = "";
            txtCity.Text = "";
            txtPostcode.Text = "";
            txtID.Text = "";
            dtpDOB.Value = DateTime.Now;

            dateSelected = false;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            core.Utils.openWindow(this, new frmMain());
        }

        private void frmPatientFind_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void frmPatientFind_Load_1(object sender, EventArgs e)
        {

        }
    }
}

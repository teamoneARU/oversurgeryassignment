﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OverSurgeryAssignment
{
    class DBConnection
    {
        //attributes
        private static DBConnection _instance;
        private static string connectionString;
        private SqlConnection connectionToDB;
        private SqlDataAdapter dataAdapter;

        //properties
        public static string ConnectionStr {
            set {
                connectionString = value;
            }
        }

        //methods
        public static DBConnection getDBConnectionInstance()
        {
            if (_instance == null)
                _instance = new DBConnection();

            return _instance;
        }

        // Open the connection
        public void openConnection()
        {
            // create the connection to the database as an instance of SqlConnection
            connectionToDB = new SqlConnection(connectionString);

            //open connection
            connectionToDB.Open();
        }

        public void closeConnection()
        {
            connectionToDB.Close();
        }

        //create a data set for a certain request
        public DataSet getDataSet(String sqlStatement, Dictionary<string, string> parameters = null)
        {
            DataSet dataSet = new DataSet();

            //open connection
            openConnection();

            //create the data adapter object
            dataAdapter = new SqlDataAdapter(sqlStatement, connectionToDB);

            if (parameters != null) {
                foreach(KeyValuePair<string, string> parameter in parameters)

                dataAdapter.SelectCommand.Parameters.Add(new SqlParameter(parameter.Key, parameter.Value));
            }

            //fill in the data set
            dataAdapter.Fill(dataSet);

            //close connection
            closeConnection();

            return dataSet;
        }

        public SqlDataReader getRecord(string SQL, Dictionary<string, string> parameters)
        {

            SqlCommand command = new SqlCommand(SQL, connectionToDB);

            // Pass in the username as a parameter into the sql query
            foreach (KeyValuePair<string, string> entry in parameters) {
                string key = entry.Key;
                string value = entry.Value;

                command.Parameters.Add(new SqlParameter(key, value));
            }

            // Execute the query and return the results into a data reader. 
            SqlDataReader rtnReader = command.ExecuteReader();
            return rtnReader;
        }

        public SqlDataReader getPatient(string SQL, Patient p)
        {
            StringBuilder where = new StringBuilder(" WHERE");
            where.Append((p.PatientID == null) ? " patientID IS NULL OR" : " patientID = '" + p.PatientID.ToString() + "' OR");
            where.Append((string.IsNullOrEmpty(p.FirstName)) ? " FirstName IS NULL OR" : " FirstName = '" + p.FirstName + "' OR");
            where.Append((string.IsNullOrEmpty(p.LastName)) ? " LastName IS NULL OR" : " LastName = '" + p.LastName + "' OR");
            where.Append((string.IsNullOrEmpty(p.Addressline1)) ? " Addressline1 IS NULL OR" : " Addressline1 = '" + p.Addressline1 + "' OR");
            where.Append((string.IsNullOrEmpty(p.City)) ? " Town IS NULL OR" : " Town = '" + p.City + "' OR");
            where.Append((string.IsNullOrEmpty(p.Postcode)) ? " Postcode IS NULL OR" : " Postcode = '" + p.Postcode + "' OR");
            where.Append((p.DOB.HasValue) ? " DateOfBirth = '" + p.DOB.Value.ToString("yyyy-MM-dd") + "'" : " DateOfBirth IS NULL");

            SqlCommand command = new SqlCommand(SQL + where.ToString(), connectionToDB);

            // Execute the query and return the results into a data reader. 
            SqlDataReader rtnReader = command.ExecuteReader();
            return rtnReader;
        }

        public void updatePatient(String SQL, Dictionary<string, string> parameters)
        {
            SqlCommand command = new SqlCommand(SQL, connectionToDB);

            foreach (KeyValuePair<string, string> entry in parameters) {
                String key = entry.Key;
                String value = entry.Value;

                command.Parameters.Add(new SqlParameter(key, value));
            }

            command.ExecuteNonQuery();
        }

        //for inserting a new user from frmCreateUser 
        public void insertNewUser(string sqlQuery, String username, String password)
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = sqlQuery;
            command.Parameters.Add(new SqlParameter("Username", username));
            command.Parameters.Add(new SqlParameter("Password", password));

            openConnection();
            command.Connection = connectionToDB;

            int noRows = command.ExecuteNonQuery();

            closeConnection();

            Console.WriteLine("n-" + noRows);
        }

        public int insertNewPatient(string sql, Dictionary<string, string> parameters)
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = sql;
            command.Connection = connectionToDB;

            foreach (KeyValuePair<string, string> entry in parameters) {
                command.Parameters.Add(new SqlParameter(entry.Key, entry.Value));
            }

            return command.ExecuteNonQuery();
        }

        public void insertAppointment(string sqlQuery, Dictionary<string, int> parameters)
        {
            SqlCommand command = new SqlCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = sqlQuery;

            foreach (KeyValuePair<string, int> item in parameters) {
                command.Parameters.Add(new SqlParameter(item.Key, item.Value));
            }

            command.Connection = connectionToDB;

            int noRows = command.ExecuteNonQuery();
        }

        public void updatePrescription(String SQL)
        {
            SqlCommand command = new SqlCommand(SQL);

            command.Connection = connectionToDB;

            command.ExecuteNonQuery();
        }

        public SqlDataReader getPatientTests(String sql, Dictionary<string, string> parameters) {
            SqlCommand command = new SqlCommand(sql);
            command.Connection = connectionToDB;

            foreach (KeyValuePair<string, string> parameter in parameters) {
                command.Parameters.Add(new SqlParameter(parameter.Key, parameter.Value));
            }

            return command.ExecuteReader();
        }

        public bool isPrescriptionExtendable(string sql, int prescriptionID) {
            SqlCommand command = new SqlCommand(sql);
            command.Connection = connectionToDB;

            command.Parameters.Add(new SqlParameter("@id", prescriptionID.ToString()));

            SqlDataReader reader = command.ExecuteReader();
            DataTable table = new DataTable();
            table.Load(reader);

            string extendable = table.Rows[0]["Extendable"].ToString();
            bool isExtenable = false;

            switch (extendable) {
                case "Y":
                    isExtenable = true;
                    break;
                case "N":
                    break;

            }

            return isExtenable;
        }
    }
}